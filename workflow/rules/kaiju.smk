rule kaiju:
    input:
        R1="results/trimmed/{sample}_1.fastq.gz",
        R2="results/trimmed/{sample}_2.fastq.gz",
        nodes="{kaiju_db}nodes.dmp".format(**config),
        names="{kaiju_db}names.dmp".format(**config),
        database="{kaiju_db}kaiju_db_nr_euk.fmi".format(**config),
    output:
        kaiju="results/kaiju/{sample}/{sample}.kaiju",
        krona="results/kaiju/{sample}/{sample}.krona",
        tsv="results/kaiju/{sample}/{sample}-taxNames.tsv",
        html=temp("results/kaiju/{sample}/{sample}.html"),
    threads: 24
    log:
        "logs/kaiju/{sample}.log",
    shell:
        """
        conda activate kaiju-1.9.2
        kaiju -t {input.nodes} -f {input.database} -i {input.R1} -j {input.R2} -o {output.kaiju} -z {threads} -v > {log}
        sleep 10
        kaiju2krona -t {input.nodes} -n {input.names} -i {output.kaiju} -o {output.krona} -u -v >> {log}
        kaiju-addTaxonNames -t {input.nodes} -n {input.names} -i {output.kaiju} -o {output.tsv} -r superkingdom,phylum,class,order,family,genus,species -v >> {log}
        conda deactivate
        
        conda activate krona-2.8
        ktImportText -o {output.html} {output.krona} >> {log}
        conda deactivate
        """


rule kronaHTML:
    input:
        expand("results/kaiju/{sample}/{sample}.krona", sample=SAMPLES.index),
    output:
        html="results/kaiju/krona.html",
    threads: 4
    log:
        "logs/kaiju/krona.log",
    shell:
        """
        conda activate krona-2.8
        ktImportText -o {output.html} {input} > {log}
        conda deactivate
        """


rule kaiju2table:
    input:
        kaiju=expand("results/kaiju/{sample}/{sample}.kaiju", sample=SAMPLES.index),
        nodes="{kaiju_db}nodes.dmp".format(**config),
        names="{kaiju_db}names.dmp".format(**config),
        database="{kaiju_db}kaiju_db_nr_euk.fmi".format(**config),
    output:
        tsv="results/kaiju/kaiju.tsv",
    threads: 4
    log:
        "logs/kaiju/kaiju2table.log",
    shell:
        """
        conda activate kaiju-1.9.2
        kaiju2table -t {input.nodes} -n {input.names} -r species -m 5 -u -v -o {output.tsv} {input.kaiju} >> {log}
        conda deactivate
        """


# library(tidyverse)

# table <- vroom::vroom("results/kaiju/kaiju.tsv", delim = "\t", col_types = "fddif") |>
#     mutate(sample = as_factor(stringr::str_split_i(file, pattern = "/", 3)), .before = 1, .keep = "unused") |>
#     mutate(taxon_name = fct_reorder(taxon_name, percent, .desc = TRUE, .na_rm = FALSE))

# ggplot(table, aes(fill = taxon_name, y = reads, x = sample)) +
#     geom_bar(position = "fill", stat = "identity") +
#     scale_fill_brewer(palette = "Set1") +
#     theme(legend.position = "bottom") +
#     guides(fill = guide_legend(ncol = 2)) +
#     theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1))
# ggsave("plot.png", width = 10, height = 12)
